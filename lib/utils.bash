#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/rstudio/rstudio"
TOOL_NAME="r-studio"
TOOL_TEST="r-studio"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" |
    grep -o 'refs/tags/.*' | cut -d/ -f3- |
    sed 's/^v//' | sed 's/\+/-/'
}

list_all_versions() {
  list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -rf "$filename" &
  echo "Getting version: $version"
  url="https://download1.rstudio.org/desktop/bionic/amd64/rstudio-${version}-amd64-debian.tar.gz"
  url2="https://download1.rstudio.org/electron/jammy/amd64/rstudio-${version}-amd64-debian.tar.gz"
  y=$(echo "$version" | cut -d"." -f1)
  m=$(echo "$version" | cut -d"." -f2)
  if [ $y$m -gt 202207 ]; then
    echo "Downloading $url2"
    curl "${curl_opts[@]}" -o "$filename" -C - "$url2" || fail "Could not download $url2"
  else
    echo "Downloading $url"
    curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  fi
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"

    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    
    # HACK!:
    touch "$install_path/bin/r-studio"
    if [ -f "$install_path/bin/rstudio" ]; then
      echo "$install_path/bin/rstudio --no-sandbox \"\$@\"" >> "$install_path/bin/r-studio"
    else
      echo "$install_path/rstudio" >> "$install_path/bin/r-studio"
    fi
    chmod a+x "$install_path/bin/r-studio"
    if [ -f "$install_path/bin/rstudio" ]; then
      echo "$install_path/bin/rstudio exists.."
    else
      # Hack for backwards compatibility
      cp "$install_path/bin/r-studio" "$install_path/bin/rstudio"
    fi

    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
